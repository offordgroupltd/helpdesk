const fs = require('fs');
const Discord = require('discord.js');
const mysql = require('mysql');
const config = require('./config.json');
const util = require('util');

const client = new Discord.Client();
client.commands = new Discord.Collection();

fs.readdir('./cmds', (err, files) => {
	if(err) console.error(err);

	let commandFiles = files.filter(f => f.endsWith('.js'));
	if(commandFiles.length <= 0) {
		console.log('No commands found.');
		return;
	}

	if(commandFiles.length === 1) {
		console.log(`Loading 1 command!`)
	} else {
		console.log(`Loading ${commandFiles.length} command(s)!`)
	}

	commandFiles.forEach((f, i) => {
		let command = require(`./cmds/${f}`);
		console.log(`${i + 1}: ${command.help.name} command loaded!`);
		client.commands.set(command.help.name, command);
	});
});

var con = mysql.createConnection({
	host: config.db_host,
	user: config.db_user,
	password: config.db_pass,
	database: config.db_database
});

con.connect(err => {
	if(err) throw err;
});

const cooldowns = new Discord.Collection();

client.once('ready', () => {
	console.log(`${client.user.tag} has now been started and is ready!`);
    client.user.setPresence({ status: 'online', game: { name: '-help | discord.offordgroup.co/help-desk'}});
});

client.on("guildCreate", server => {
    const serverOwner = client.users.get(server.ownerID);

    serverOwner.send(`Hello! You are recieveing this message because you or one of your admins/staff added ${client.user.tag} Bot to the server ${server.name}. To see what I can do reply with '-help'. To set up the bot on your server type -setup.`);
    console.log(`${client.user.tag} was just added to the server ${server.name}! Welcome message was sent to ${serverOwner.username}#${serverOwner.discriminator}.`);

	con.query(`
        INSERT INTO \`servers\` (serverId, prefix, nextTicket, setupDone)
        VALUES (?, '-', 1, false)
    `, [`${server.id}`]);
})

client.on('message', message => {
	if (message.channel.type !== 'text') {
		let prefix = '-';
		if (!message.content.startsWith(prefix) || message.author.bot) return;

		const args = message.content.slice(prefix.length).split(/ +/);
		const commandName = args.shift().toLowerCase();

		// Hidden debug command
		if (commandName === 'debug') {
			if (args[0] === 'message') {
				console.log(message);
			} else if (args[0] === 'server') {
				console.log(server);
			}
			message.reply(`Check bot console for ${args[0]} debug info!`);
		}

		const command = client.commands.get(commandName)
			|| client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

		if (!command) return;

		if (command.help.guildOnly && message.channel.type !== 'text') {
			return message.reply('I can\'t execute that command inside DMs!');
		}

		if (command.help.args && !args.length) {
			let reply = `You didn't provide any arguments, ${message.author}!`;

			if (command.help.usage) {
				reply += `\nThe proper usage would be: \`${prefix}${command.help.name} ${command.help.usage}\``;
			}

			return message.channel.send(reply);
		}

		if (!cooldowns.has(command.help.name)) {
			cooldowns.set(command.help.name, new Discord.Collection());
		}

		const now = Date.now();
		const timestamps = cooldowns.get(command.help.name);
		const cooldownAmount = (command.help.cooldown || 3) * 1000;

		if (timestamps.has(message.author.id)) {
			const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

			if (now < expirationTime) {
				const timeLeft = (expirationTime - now) / 1000;
				return message.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.help.name}\` command.`);
			}
		}

		timestamps.set(message.author.id, now);
		setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

		try {
			command.execute(message, args);
		} catch (error) {
			console.error(error);
			message.reply('There was an error trying to execute that command! Please add Lexi#5873 and report this error message to her.');
		}
	} else {
		const server = message.guild;
		con.query(`SELECT \`prefix\` FROM \`servers\` WHERE \`serverId\` = '${server.id}'`, (err, rows) => {
			if(err) throw err;
			const prefix = rows[0].prefix;

			if (!message.content.startsWith(prefix) || message.author.bot) return;

			const args = message.content.slice(prefix.length).split(/ +/);
			const commandName = args.shift().toLowerCase();

			// Hidden debug command
			if (commandName === 'debug') {
				if (args[0] === 'message') {
					console.log(message);
				} else if (args[0] === 'server') {
					console.log(server);
				}
				message.reply(`Check bot console for ${args[0]} debug info!`);
			}

			const command = client.commands.get(commandName)
				|| client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

			if (!command) return;

			if (command.help.guildOnly && message.channel.type !== 'text') {
				return message.reply('I can\'t execute that command inside DMs!');
			}

			if (command.help.args && !args.length) {
				let reply = `You didn't provide any arguments, ${message.author}!`;

				if (command.help.usage) {
					reply += `\nThe proper usage would be: \`${prefix}${command.help.name} ${command.help.usage}\``;
				}

				return message.channel.send(reply);
			}

			if (!cooldowns.has(command.help.name)) {
				cooldowns.set(command.help.name, new Discord.Collection());
			}

			const now = Date.now();
			const timestamps = cooldowns.get(command.help.name);
			const cooldownAmount = (command.help.cooldown || 3) * 1000;

			if (timestamps.has(message.author.id)) {
				const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

				if (now < expirationTime) {
					const timeLeft = (expirationTime - now) / 1000;
					return message.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.help.name}\` command.`);
				}
			}

			timestamps.set(message.author.id, now);
			setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

			try {
				command.execute(message, args, con, server, client);
			} catch (error) {
				console.error(error);
				message.reply('There was an error trying to execute that command! Please add Lexi#5873 and report this error message to her.');
			}
		});
	}
});

client.login(config.token);
