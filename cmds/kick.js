module.exports.execute = async (message, args, con, server) => {
	if (!message.mentions.users.size) {
		return message.reply('you need to tag a user in order to kick them!');
	}

	const taggedUser = message.mentions.users.first();

	message.channel.send(`You wanted to kick: ${taggedUser.username}`);
};

module.exports.help = {
	name: 'kick',
	description: 'Tag a member and kick them (but not really).',
    guildOnly: true
};