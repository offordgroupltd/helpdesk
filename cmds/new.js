module.exports.execute = async (message, args, con, server) => {
	// server.createChannel('test-channel');
	// message.reply('Channel created!');
	message.channel.send('Command successful!');
};

module.exports.help = {
	name: 'new',
	description: 'Create a new test channel.',
	cooldown: 5,
    guildOnly: true
};