module.exports.execute = async (message, args, con, server) => {
    message.reply('Help Desk now in setup mode. I will ask some questions to get you set up!');

    // The below creates a new table with the server ID as the table name.
    con.query(`CREATE TABLE IF NOT EXISTS \`${server.id}\` (
        serverId VARCHAR(18),
        ticketId VARCHAR(5),
        ticketMembers BLOB,
        ticketArchive MEDIUMBLOB
    )`);

    message.reply('What command prefix would you like? (default is -)');

    let msgs = await message.channel.awaitMessages(msg => msg.author.id === message.author.id, {max: 1});

    let prefix = msgs.map(msg => msg.content);

    message.reply('What category would you like new tickets to be created under? (I will make it if it doesn\'t exist!).');

    msgs = await message.channel.awaitMessages(msg => msg.author.id === message.author.id, {max: 1});

    let category = msgs.map(msg => msg.content);

    message.reply('What role would you like to give to your support staff? (I will make it if it doesn\'t exist!).');

    msgs = await message.channel.awaitMessages(msg => msg.author.id === message.author.id, {max: 1});

    let role = msgs.map(msg => msg.content);


    let createdRole = await server.createRole({
       name: `${role}`,
       color: 'BLUE',
       permissions: 0
    });


    let createdCategory = await server.createChannel(`${category}`, {
        type: 'category'
    });

    let createdChannel = await server.createChannel('new ticket', {
        parent: `${createdCategory.id}`
    });

    let sql = `UPDATE \`servers\` SET \`prefix\` = '${prefix}', \`categoryId\` = '${createdCategory.id}', \`roleId\` = '${createdRole.id}', \`channelId\` = '${createdChannel.id}' WHERE \`serverId\` = '${server.id}'`;

    console.log(sql);

    await con.query(sql);

    con.end();

    message.reply(`Help Bot setup complete! The prefix has been set to '${prefix}'. The new ticket category is '${category}'. The channel for new tickets has been created and is called 'new-ticket', it's under the '${category}' category. The new role '${role}' has been created. If you wish to remove this bot from your server type the command '${prefix}remove'. It will delete the category and role but the ticket history will remain!`);
};

module.exports.help = {
    name: 'setup',
    description: 'Sets up all Help Desk related channels and roles',
    guildOnly: true
};