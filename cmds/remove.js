module.exports.execute = async (message, args, con, server, client) => {
    con.query(`SELECT * FROM \`servers\` WHERE \`serverId\` = '${server.id}'`, (err, rows) => {
        if(err) throw err;

        const categoryId = rows[0].categoryId;
        const channelId = rows[0].channelId;
        const roleId = rows[0].roleId;

        if (categoryId != '') {
            client.channels.get(`${categoryId}`).delete();
        }

        if (channelId != '') {
            client.channels.get(`${channelId}`).delete();
        }

        if (roleId != '') {
            server.roles.get(`${roleId}`).delete();
        }

        let sql = `UPDATE \`servers\` SET \`prefix\` = '-', \`categoryId\` = '', \`roleId\` = '', \`channelId\` = '' WHERE \`serverId\` = '${server.id}'`;

        con.query(sql);

        message.reply("I would have left by now if it wasn't for testing...");
        // server.leave();
    });
};

module.exports.help = {
    name: 'remove',
    description: 'Deletes all Help Desk related channels and roles then kicks the bot from the server.',
    guildOnly: true
};